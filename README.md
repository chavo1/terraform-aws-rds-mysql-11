# terraform-aws-rds-mysql-0.12.0

A terraform module to create an RDS MySQL Database.
The module generates a user and a password to use the database and stores it in
AWS Secrets Manager under `database/mysql/<db_identifier>/auth`.

Check the `auth` output on this README for the ARN and format.

The connection information is stored in AWS Systems Manager Parameter Store using the
following names:

- `database/mysql/<db_identifier>/address` - the DB DNS entry
- `database/mysql/<db_identifier>/port` - the port to connect on
- `database/mysql/<db_identifier>/endpoint` - the full endpoint as `address:port`

If a replica is specified its connection information is also published in the same
way using the replica identifier.

## Disk Sizes

The database is created at the given size but with a maximum size set to 110% of the size
given. This gives some flexibility for RDS to increase the size of the disk should the
limit be hit.

The disk space for the replica database is set to 120% of the primary database. It also
has a maximum size of 110% of its disk.

For example:

If we create a database with the default 50GB disk the maximum size will be 55GB.
A replica of this would be sized at 60GB with a maximum size of 66GB.

**Note:**
There are constraints on the allowed database instance types dependent on the MySQL
version selected. These can be found here: 
https://docs.aws.amazon.com/AmazonRDS/latest/UserGuide/Concepts.DBInstanceClass.html.

## Module Input Variables

- `db_identifier` - The name of the database instance.
- `environment` - The name of the environment the network will hold.
- `vpc_id` - The VPC the Database should be created in.
- `private_subnet_ids` - List of private subnets IDs to build the database in.
- `db_instance_class` - (Optional) The AWS database instance class. Defaults to `db.t3.small`.
- `read_replica` - (Optional) Whether to create a read replica. Defaults to `false`.
- `read_replica_instance_class` - (Optional) The AWS replica database instance class. Defaults to `db.t3.small`.
- `db_name` - (Optional) The name of the database to create in the instance. Defaults to `db_identifier`.
- `db_version` - (Optional) The MySQL version. Defaults to `5.7`.
- `db_storage` - (Optional) The storage reserved for the DB in GB. Defaults to `50`.
- `multi_az` - (Optional) Is the DB replicated across AZ. Defaults to `true`.
- `deletion_protection` - (Optional) Should the database be protected against deletion. Defaults to `false`.
- `apply_immediately` - (Optional) Apply DB changes immediately. If true will generate disruption. Defaults to `false`.
- `port` - (Optional) Port to accept requests on. Defaults to `3306`.
- `iops` - (Optional) The IOPs required for the database. Defaults to `null`.
- `username` - (Optional) Master DB username. The password is automatically generated and stored in AWS Secrets Manager. Defaults to `root`.
- `backup_retention` - (Optional) The number of days to retain backups. Defaults to `3`.
- `log_retention` - (Optional) The number of days to retain logs. Defaults to `30`.
- `allow_major_version_upgrade` - (Optional) Should major version upgrades be applied. Defaults to `false`.
- `tags` - (Optional) Additional tags for all resources. Defaults to `none`.

## Usage

```hcl
module "rds-mysql" {
  source  = "app.terraform.io/wallbox/rds-mysql/aws"
  version = "0.10.0"

  environment        = "dev"
  vpc_id             = "<vpc_id>"
  private_subnet_ids = "<private_subnet_ids>"
  username           = "<username>"
  db_identifier      = "cloud"

  tags = {
    'CostCentre' = 'development'
    'Project'    = 'pr/001'
  }

}
```

# Outputs

- `arn` - The ARN of the database instance.
- `address` - The hostname of the database instance.
- `port` - The port used.
- `identifier` - The database identifier.
- `name` - The database name.
- `auth` - The database AWS Secrets Manager ARN.
- `replica_arn` - The ARN of the replica database instance.
- `replica_address` - The hostname of the replica database instance.
- `replica_port` - The port used by the replica.
- `replica_identifier` - The replica database identifier.
- `replica_name` - The replica database name.

# Authors

techops@wallbox.com
