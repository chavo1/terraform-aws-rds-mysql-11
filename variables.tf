variable "db_identifier" {
  type        = string
  description = "The name of the database instance."
}

variable "environment" {
  type        = string
  description = "(Optional) The name of the environment the network will hold."
  default     = "unknown"
}

variable "vpc_id" {
  type        = string
  description = "The id of the VPC to build the database in."
}

variable "private_subnet_ids" {
  type        = list(string)
  description = "List of private subnets IDs to build the database in."
}

variable "db_instance_class" {
  type        = string
  description = "(Optional) The AWS database instance class. Defaults to `db.t3.small`."
  default     = "db.t3.small"
}
variable "read_replica" {
  type        = bool
  description = "(Optional) Whether to create a read replica. Defaults to `false`."
  default     = false
}
variable "read_replica_instance_class" {
  type        = string
  description = "(Optional) The AWS replica database instance class. Defaults to `db.t3.small`."
  default     = "db.t3.small"
}

variable "db_name" {
  type        = string
  description = "(Optional) The name of the database to create in the instance. Defaults to `db_identifier`."
  default     = null
}

variable "db_version" {
  type        = string
  description = "(Optional) The MySQL version. Defaults to `5.7`."
  default     = "5.7"
}

variable "db_storage" {
  type        = number
  description = "(Optional) The storage reserved for the DB in GB. Defaults to `50`."
  default     = 50
}

variable "multi_az" {
  type        = bool
  description = "(Optional) Is the DB replicated across AZ. Defaults to `true`."
  default     = true
}

variable "deletion_protection" {
  type        = bool
  description = "(Optional) Should the database be protected against deletion. Defaults to `false`."
  default     = false
}

variable "apply_immediately" {
  type        = bool
  description = "(Optional) Apply DB changes immediately. If true will generate disruption. Defaults to `false`."
  default     = false
}

variable "port" {
  type        = number
  description = "(Optional) Port to accept requests on. Defaults to `3306`."
  default     = 3306
}

variable "iops" {
  type        = number
  description = "(Optional) The IOPs required for the database. Defaults to `null`."
  default     = null
}

variable "username" {
  type        = string
  description = "(Optional) Master DB username. The password is automatically generated and stored in AWS Secrets Manager. Defaults to `root`."
  default     = "root"
}

variable "backup_retention" {
  type        = number
  description = "(Optional) The number of days to retain backups. Defaults to `3`."
  default     = 3
}

variable "log_retention" {
  type        = number
  description = "(Optional) The number of days to retain logs. Defaults to `30`."
  default     = 30
}

variable "allow_major_version_upgrade" {
  type        = bool
  description = "(Optional) Should major version upgrades be applied. Defaults to `false`."
  default     = false
}

variable "tags" {
  type        = map
  description = "(Optional) Additional tags for all resources. Defaults to `none`."
  default     = {}
}
